(ns env.index
  (:require [env.dev :as dev]))

;; undo main.js goog preamble hack
(set! js/window.goog js/undefined)

(-> (js/require "figwheel-bridge")
    (.withModules #js {"./assets/icons/loading.png" (js/require "../../../assets/icons/loading.png"), "expo" (js/require "expo"), "./assets/images/cljs.png" (js/require "../../../assets/images/cljs.png"), "./assets/icons/app.png" (js/require "../../../assets/icons/app.png"), "react-native" (js/require "react-native"), "react" (js/require "react"), "react-native-material-dropdown" (js/require "react-native-material-dropdown"), "create-react-class" (js/require "create-react-class"), "./assets/images/star_wars_clear.png" (js/require "../../../assets/images/star_wars_clear.png"), "react-native-star-wars" (js/require "react-native-star-wars")}
)
    (.start "main" "expo" "192.168.43.188"))
