(ns star-wars-app.controller
    (:require [star-wars-app.model :as model]
              [cljs.core.async :as a :refer [chan <! put!]]
              [ajax.core :refer [ajax-request json-response-format]]
              [star-wars-app.model :as model])
    (:require-macros [cljs.core.async.macros :refer [go]]))


(defn GET<
  ([url]
    (GET< url (fn [[ok resp]]
           resp))
    )
  ([url from-response]
    (print "GET" url)
    (let [ch (chan)]
    (ajax-request
     {:uri url
      :method :get
      :handler (fn [resp]
                   (put! ch (from-response resp)))
      :response-format (json-response-format {:keywords? true})})
    ch)))



(defn get-movies-name [data]
  (let [result (:results data)
        state @model/app-state]
        (if (empty? result)
          (str "error")
          (-> state
            (assoc :titles (into []
                            (for [res (sort-by :release_date result)]
                              (assoc {} :value (:title res)))))
            (model/swapm! model/app-state))
      )))


(defn get-characters-details [char]
  (let [state @model/app-state]
    (-> state
      (assoc-in [:chars (:name char)] {:name (:name char)
                                       :gender (:gender char)
                                       :height (:height char)})
      (assoc-in [:gender (:name char)] {:value (:gender char)})
      (model/swapm! model/app-state))
      ))


(defn get-data-from-selected-movie [data]
 (let [result (:results data)
        sel (:selected-movie @model/app-state)]
        (if (empty? result)
          (str "error")
          (filter #(= (:title %) sel) result))))


(defn fetch-data [f link]
   (let [c (chan)]
       (go
           (put! c (f
             (<!
               (GET<
                 (str link))))))
                 c))


(defn get-movies []
 (go
   (let [v (<! (fetch-data get-movies-name "https://swapi.co/api/films/"))]
    (if (= v "error")
     (js/alert "error on fetching data")
     (print "ok!")))))


(defn list-of-characters [chars]
  (go
    (loop [char chars]
      (if (empty? char)
        (swap! model/app-state assoc :indicator false)
        (recur (do (<! (fetch-data get-characters-details (first char))) (rest char)))))))


(defn get-data []
    (let [movie-channel (chan)]
      (go
        (put! movie-channel (first (<! (fetch-data get-data-from-selected-movie "https://swapi.co/api/films/")))))
          movie-channel))

(defn get-characters []
  (go
    (let [movie-data (<! (get-data))
          chars (:characters movie-data)]
      (list-of-characters chars))))


(defn get-opening-crawl []
  (go
   (let [movie-data (<! (get-data))]
    (reset! model/opening-crawl (:opening_crawl movie-data)))))


(defn change-movie [movie]
    (if (= (:context @model/app-state) "logo")
      (do (-> @model/app-state
            (assoc :selected-movie movie)
            (assoc :context "movie")
            (assoc :indicator true)
            (assoc :chars nil)
            (assoc :gender {"Gender" {:value "Gender"}})
            (assoc :selected-gender "Gender")
            (model/swapm! model/app-state))
        (get-opening-crawl)
        (get-characters))
        ;;else
      (do (-> @model/app-state
            (assoc :selected-movie movie)
            (assoc :indicator true)
            (assoc :chars nil)
            (assoc :gender {"Gender" {:value "Gender"}})
            (assoc :selected-gender "Gender")
            (model/swapm! model/app-state))
          (get-opening-crawl)
          (get-characters))))
