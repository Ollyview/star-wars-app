(ns star-wars-app.core
    (:require [reagent.core :as r :refer [atom]]
              [re-frame.core :refer [subscribe dispatch dispatch-sync]]
              [oops.core :refer [ocall]]
              [star-wars-app.model :as m :refer [app-state]]
              [star-wars-app.controller :as controller]
              [star-wars-app.view :as view]
              ))


(enable-console-print!)

(def ReactNative (js/require "react-native"))
(def expo (js/require "expo"))
(def Dropdown (js/require "react-native-material-dropdown"))
(def star-wars-logo (js/require "./assets/images/star_wars_clear.png"))

(def dropdown (r/adapt-react-class (.-Dropdown Dropdown)))
(def text (r/adapt-react-class (.-Text ReactNative)))
(def view (r/adapt-react-class (.-View ReactNative)))
(def image (r/adapt-react-class (.-Image ReactNative)))

(defonce initialize (controller/get-movies))

(defn app-root []
  [view {:style {:flex 1 :background-color "black" :padding-top 24}}
    [text {:style {:font-size 24 :color "yellow" :align-self "center"}} "May the Force be with you"]
    [view {:style {:background-color "black" :margin-left 10 :margin-right 10}}
      [dropdown {:data (:titles @app-state)
                  :dropdown-position 0
                  :text-color "yellow"
                  :base-color "white"
                  :selected-item-color "gray"
                  :on-change-text #(controller/change-movie %)
                  :value (:selected-movie @app-state)}]]
    (let [context (:context @app-state)]
        (case context
          "logo" [view {:style {:justify-content "center" :flex 1 :align-items "center"}}
                  [image {:source star-wars-logo
                          :style {:width "90%"
                                  :height "33%"}}]]
          "movie" [view/scroll-view {:style {:flex 1}}
                    [view/opening-crawl]
                    [view/selected-movie]]))])

(defn init []
  (ocall expo "registerRootComponent" (r/reactify-component app-root)))
