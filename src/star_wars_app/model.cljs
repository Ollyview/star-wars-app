(ns star-wars-app.model
    (:require [reagent.core :as r :refer [atom]]))


(defonce app-state
  (atom {:titles []
         :chars nil
         :context "logo"
         :selected-movie "Select a movie"
         :sort-by-name "default"
         :gender nil
         :selected-gender "Gender"
         :indicator false
         :update false}))

(def opening-crawl (atom nil))

(defn swapm! [new old]
  (reset! old new))
