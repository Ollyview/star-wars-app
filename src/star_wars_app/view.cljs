(ns star-wars-app.view
    (:require [reagent.core :as r]
              [star-wars-app.model :as model]
              [star-wars-app.controller :as controller]
              [cljs.reader :refer [read-string]]))


(def ReactNative (js/require "react-native"))
(def StarWars (js/require "react-native-star-wars"))
(def Dropdown (js/require "react-native-material-dropdown"))
(def dropdown (r/adapt-react-class (.-Dropdown Dropdown)))
(def text (r/adapt-react-class (.-Text ReactNative)))
(def view (r/adapt-react-class (.-View ReactNative)))
(def image (r/adapt-react-class (.-Image ReactNative)))
(def touchable-opacity (r/adapt-react-class (.-TouchableOpacity ReactNative)))
(def activity-indicator (r/adapt-react-class (.-ActivityIndicator ReactNative)))
(def scroll-view (r/adapt-react-class (.-ScrollView ReactNative)))
(def easing (.-Easing ReactNative))
(def animated (.-Animated ReactNative))
(def animated-value (.-Value animated))
(def animated-view (r/adapt-react-class (.-View animated)))


(def head-style {:color "yellow"
                 :font-size 28
                 :text-align "center"})

(def text-style {:color "yellow"
                 :font-size 17
                 :text-align "center"})


(defn filter-characters [characters]
  (if (= (:selected-gender @model/app-state) "Gender")
    characters
    (filter #(= (:gender %) (:selected-gender @model/app-state)) characters)))


(defn table-footer []
  (let [chars (filter-characters (vals (:chars @model/app-state)))
        heights (reduce (fn [acc num] (conj acc (when (number? (read-string (:height num))) (read-string (:height num))))) [] chars)
        sum (reduce + heights)]
    (if (:indicator @model/app-state)
      [view {:style {:flex 1 :justify-content "center" :align-items "center"}}
        [activity-indicator {:size "large" :color "yellow"}]]
      [view {:style {:flex 1}}
        [view {:style {:flex 1 :flex-direction "row" :border-top-width 1 :border-color "yellow"}}
          [view
            [text {:style text-style} "Number of characters: "]]
          [view
            [text {:style text-style} (count chars)]]]
        [view {:style {:flex 1 :flex-direction "row"}}
          [view
            [text {:style text-style} "Total height: "]]
          [view
            [text {:style text-style} (str sum "cm/")]]
          [view
            [text {:style text-style} (str (/ sum 2.54) "in")]]]])))


(defn show-characters []
  (let [chars (filter-characters (vals (:chars @model/app-state)))]
    (into [view {:style {:flex 1}}]
       (map #(do [view {:style {:flex-direction "row" :flex 1 :justify-content "center"}}
                  [view {:style {:flex 1}}
                    [text {:style text-style} (:name %)]]
                  [view {:style {:flex 1}}
                    [text {:style text-style} (:height %)]]
                  [view {:style {:flex 1}}
                    [text {:style text-style} (:gender %)]]]) (case (:sort-by-name @model/app-state)
                                                                  "ascending" (sort < chars)
                                                                  "descending" (sort > chars)
                                                                  "default" chars)))))

(defn sort-characters []
  (case (:sort-by-name @model/app-state)
      "default" "ascending"
      "ascending" "descending"
      "descending" "ascending"))


(defn table-header []
  (let [gender (distinct (vals (:gender @model/app-state)))]
    [view {:style {:flex-direction "row" :flex 1 :border-bottom-width 1 :border-color "yellow"}}
                [view {:style {:flex 1 :align-self "center"}}
                  [touchable-opacity {:on-press #(swap! model/app-state assoc :sort-by-name (sort-characters))}
                    [text {:style head-style} "Name"]]]
                [view {:style {:flex 1 :align-self "center"}}
                  [text {:style head-style} "Height"]]
                (if (:indicator @model/app-state)
                  [view {:style {:flex 1 :justify-content "center" :align-items "center"}}
                    [activity-indicator {:size "large" :color "yellow"}]]
                  [view {:style {:flex 1}}
                    [dropdown {:data gender
                                :dropdown-position 0
                                :text-color "yellow"
                                :font-size 28
                                :dropdown-offset {:top 17 :left 0}
                                :selected-item-color "gray"
                                :on-change-text #(swap! model/app-state assoc :selected-gender %)
                                :value (:selected-gender @model/app-state)}]])]))


(defn opening-crawl []
  (let [fade-in (new animated-value 0)
        x-value (new animated-value 0)]
    (.setValue fade-in 0)
    (.setValue x-value 200)
    (-> fade-in
        (animated.timing #js {:toValue 0.8 :duration 1000})
        (.start))
    (-> x-value
      (animated.timing #js {:toValue -450 :duration 30000 :asing easing})
      (.start))
    [view {:background-color "black"
           :height 200
           :width "100%"}
      [animated-view {:style {:top x-value
                              :opacity fade-in}}
          [text {:style text-style} @model/opening-crawl]]]))


(defn selected-movie []
  [view {:style {:flex 1}}
      [table-header]
      [show-characters]
      [table-footer]])
